class Response < ApplicationRecord
  belongs_to :topic

  validates_presence_of :author_name
  validates_presence_of :content
  validates_presence_of :topic_id
  validates_numericality_of :topic_id
  
end
