class Topic < ApplicationRecord
  has_many :responses

  validates_presence_of :author_name
  validates_presence_of :content

  def responses_count
    self.responses.count
  end
end
