class ResponsesController < ApplicationController
  before_action :set_topic, only: [:index]

  def index
    @responses = @topic.responses.order(id: :desc)
    if @responses.empty?
      render json: {"error": "Aún no hay respuestas"}
    else
      render json: @responses
    end
  end

  def create
    @response = Response.new(response_params)

    if @response.save
      render json: @response, status: :created
    else
      render json: @response.errors, status: :unprocessable_entity
    end
  end

  private
  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  # Only allow a trusted parameter "white list" through.
  def response_params
    params.permit(:author_name,:content,:topic_id)
  end
end
