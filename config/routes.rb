Rails.application.routes.draw do
  get 'statics/index'

  resources :topics, only:[:index,:create,:show] do
    resources :responses, only:[:index,:create]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'statics#index'
end
