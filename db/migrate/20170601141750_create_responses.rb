class CreateResponses < ActiveRecord::Migration[5.1]
  def change
    create_table :responses do |t|
      t.string :author_name
      t.string :content
      t.integer :topic_id

      t.timestamps
    end
  end
end
