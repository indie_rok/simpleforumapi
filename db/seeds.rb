# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

topics = Topic.create([
    {
      author_name: "Emmanuel",
      content: "Miren esta canción, esta super cool"
    },
    {
      author_name: "Andrea",
      content: "¿Cómo dejar el drama en tu vida?"
    }
])


Response.create([
    {
      author_name: "Amalia",
      content:"Suena bien! De donde es",
      topic_id: topics.first.id
    },
    {
      author_name: "Ado",
      content:"La odio!",
      topic_id: topics.first.id
    },
    {
      author_name: "Jimena",
      content:"Tienes toda la razón. Muchas veces no lo necesitamos",
      topic_id: topics.second.id
    },
    {
      author_name: "Ado",
      content:"<3 Amo el drama",
      topic_id: topics.second.id
    }
])
